import { ToDoListDarkTheme } from "./DarkTheme";
import { ToDoListLightTheme } from "./LightTheme";
import { ToDoListPrimaryTheme } from "./PrimaryTheme";

export const arrTheme = [
  {
    id: 1,
    name: "Dark Theme",
    theme: ToDoListDarkTheme,
  },
  {
    id: 2,
    name: "Light Theme",
    theme: ToDoListLightTheme,
  },
  {
    id: 3,
    name: "Primary Theme",
    theme: ToDoListPrimaryTheme,
  },
];
