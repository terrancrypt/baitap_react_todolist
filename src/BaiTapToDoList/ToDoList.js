import React, { Component } from "react";
import { connect } from "react-redux";
import { ThemeProvider } from "styled-components";
import {
  addTaskAction,
  changeThemeAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
  updateTaskAction,
} from "../redux/action/toDoListAction";
import { ADD_TASK, CHANGE_THEME } from "../redux/type/toDoListType";
import { Button } from "./Components/Button";
import { Container } from "./Components/Container";
import { Dropdown } from "./Components/Dropdown";
import { Heading1, Heading2, Heading3 } from "./Components/Heading";
import { Table, Th, Thead, Tr } from "./Components/Table";
import { Label, TextField } from "./Components/TextField";
import { ToDoListDarkTheme } from "./Theme/DarkTheme";
import { ToDoListLightTheme } from "./Theme/LightTheme";
import { ToDoListPrimaryTheme } from "./Theme/PrimaryTheme";
import { arrTheme } from "./Theme/ThemeManager";

class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th className="align-middle">{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState(
                    {
                      disabled: false,
                    },
                    () => {
                      this.props.dispatch(editTaskAction(task));
                    }
                  );
                }}
                className="ml-1"
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th className="align-middle">{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  handleChange = (e) => {
    this.setState({
      taskName: e.target.value,
    });
  };

  // Viết hàm renderTheme import ThemeManager
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  // // Life cycle phiên bảng 16 nhận vào props mới được thực thi trước render
  // componentWillReceiveProps(newProps){
  //   console.log(this.props);
  //   console.log(newProps);
  //   this.setState({
  //     taskName: newProps.taskEdit.taskName,
  //   })
  // }

  // // LifeCycle tĩnh không truy xuất được con trỏ this
  // static getDerivedStateFromProps (newProps, currentState){
  //   //newProps là props mới, props mới là this.props (không truy xuất được)
  //   // currentState: ứng với State hiện tại

  //   // hoặc trả về state mới (this.state)
  //   let newState = {...currentState, taskName: newProps.taskEdit.taskName};
  //   return newState;

  //   //trả về null thì state giữ nguyên
  //   // return null;
  // }

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              // Dispatch value lên reducer
              this.props.dispatch(changeThemeAction(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading1>To do list</Heading1>
          <TextField
            value={this.state.taskName}
            onChange={this.handleChange}
            label="Task name"
          ></TextField>
          <Button
            onClick={() => {
              // Lấy thông tin người dùng nhập vào từ input
              let taskName = this.state;

              // Tạo ra 1 task object
              let newTask = {
                id: Date.now(),
                taskName: taskName.taskName,
                done: false,
              };

              // Đưa task object lên redux thông qua phương thức dispatch
              this.props.dispatch(addTaskAction(newTask));
            }}
            className="ml-2"
          >
            <i className="fa fa-plus"></i> Add task
          </Button>
          {this.state.disabled ? (
            <Button
              disabled
              onClick={() => {
                this.props.dispatch(updateTaskAction(this.state.taskName));
              }}
              className="ml-2"
            >
              <i className="fa fa-upload"></i> Update task
            </Button>
          ) : (
            <Button
              onClick={() => {
                let {taskName} = this.state;
                this.setState(
                  {
                    disabled: true,
                    taskName: "",
                  },
                  () => {
                    this.props.dispatch(updateTaskAction(taskName));
                  }
                );
              }}
              className="ml-2"
            >
              <i className="fa fa-upload"></i> Update task
            </Button>
          )}
          <hr />
          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading3>Task completed</Heading3>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }

  // Đây là lifecycle trả về props cũ và state cũ của component trước khi render(lifecycle chạy sau render)
  componentDidUpdate(prevProps, prevState) {
    // So sánh nếu như props trước đó (taskEdit trước mà khác taskEdit hiện tai thì mình mới setState)

    if (prevProps.taskEdit.id != this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.toDoListReducer.themeToDoList,
    taskList: state.toDoListReducer.taskList,
    taskEdit: state.toDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
